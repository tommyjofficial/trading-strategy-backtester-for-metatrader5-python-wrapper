## Trading Strategy Backtester

#### Backtester for testing trading strategies using historic data from MetaTrader5 trading platform.

#### Trading strategy: scalping, using three moving averages to determine entry and exit points. Stop-loss and market closing scenarios also implemented.

### References:

#### ig.com/en/trading-strategies/four-simple-scalping-trading-strategies-190131